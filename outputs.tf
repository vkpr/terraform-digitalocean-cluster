output "cluster_endpoint" {
  description = "Endpoint da API do cluster Kubernetes."
  value       = digitalocean_kubernetes_cluster.cluster.endpoint
}

output "cluster_certificate_authority_data" {
  description = "Certificate Authority Data do cluster Kubernetes."
  value       = base64decode(digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate)
}

output "kubeconfig_token" {
  description = "Cluster token."
  value       = digitalocean_kubernetes_cluster.cluster.kube_config[0].token
}
