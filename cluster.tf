resource "digitalocean_kubernetes_cluster" "cluster" {
  name         = var.cluster_name
  region       = var.cluster_region
  auto_upgrade = var.cluster_auto_upgrade
  version      = data.digitalocean_kubernetes_versions.cluster_version.latest_version
  ha           = var.cluster_ha
  tags         = var.tags

  node_pool {
    name       = var.node_pool_default.name
    size       = var.node_pool_default.size
    node_count = var.node_pool_default.node_count
    auto_scale = var.node_pool_default.auto_scale
    min_nodes  = var.node_pool_default.min_nodes
    max_nodes  = var.node_pool_default.max_nodes
    tags       = var.tags
  }

  maintenance_policy {
    day        = var.cluster_maintenance_policy_day
    start_time = var.cluster_maintenance_policy_hour
  }
}
