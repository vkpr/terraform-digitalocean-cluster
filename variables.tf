## General
variable "tags" {
  description = "A list of tag names to be applied to all resources from the cluster."
  type        = list(string)
}

## Cluster
variable "cluster_name" {
  description = "Kubernetes Cluster Name"
  type        = string
}

variable "cluster_region" {
  description = "Region where the cluster will be deployed"
  type        = string
}

variable "prefix_version" {
  description = "Define minor version for the Kubernetes cluster"
  type        = string
}

variable "cluster_auto_upgrade" {
  description = "Sets if the cluster will auto upgrade the kubernetes version"
  type        = bool
  default     = false
}

variable "cluster_ha" {
  description = "Set high availability in kubernetes cluster"
  type        = bool
  default     = false
}

variable "cluster_maintenance_policy_day" {
  description = "Sets the day window for cluster maintenance"
  type        = string
  default     = "sunday"
}

variable "cluster_maintenance_policy_hour" {
  description = "Sets the start time window for cluster maintenance"
  type        = string
  default     = "04:00"
}

## Node Pools
variable "node_pool_default" {
  description = "Default node pool"
  type = object({
    name       = string
    size       = string
    node_count = number
    auto_scale = optional(bool)
    min_nodes  = optional(number)
    max_nodes  = optional(number)
  })
}

variable "additional_node_pools" {
  description = "Additional node pools for the cluster"
  type = map(object({
    name       = string
    size       = string
    node_count = number
    auto_scale = optional(bool)
    min_nodes  = optional(number)
    max_nodes  = optional(number)
  }))
  default = {}
}